<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['namespace' => 'api'], function () {
    // Route::get('/login', function(){
    //   $query = http_build_query([
    //     'client_id' => '7',
    //     'redirect_uri' => 'http://localhost:8000',
    //     'response_type' => 'code',
    //     'scope' => '',
    //   ]);
    //   return redirect('http://localhost:8000/oauth/authorize?'.$query);
    // });
// });

Route::group(['middleware' => 'api'], function(){

  Route::get('narrations', function(){
    return App\Narration::all();
  });

  Route::get('listicles', function(){
    return App\Listicle::all();
  });

  Route::get('narration/edit/{id}', function($id){
    return App\Narration::find($id);
  });

  Route::post('narration/save-narration', 'NarrationController@store');

  Route::put('narration/update-narration/{id}', 'NarrationController@update');

  Route::delete('narration/delete-narration/{id}', 'NarrationController@destroy');

  Route::post('listicle/save-listicle', 'ListicleController@store')->name('save-listicle');

  Route::post('listicle/save-listicle-item', 'ListicleController@store')->name('save-listicle-item');
});
