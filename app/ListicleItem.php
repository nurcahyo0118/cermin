<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListicleItem extends Model
{
  public function listicle()
  {
    return $this->belongsTo('App\Listicle');
  }
}
