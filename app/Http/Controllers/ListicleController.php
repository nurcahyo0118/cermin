<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listicle;
use App\ListicleItem;

class ListicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $listicle = new Listicle;
      $listicle->title = $request->title;
      $listicle->excerpt = $request->excerpt;
      $listicle->body = $request->body;
      $listicle->author_id = 0;


      $listicle->save();

      foreach ($request->items as $item) {
        $listicle_item = new ListicleItem;
        $listicle_item->subtitle = $item['subtitle'];
        $listicle_item->image_url = 'testt';
        $listicle_item->video_url = 'testt';
        $listicle_item->body = $item['body'];
        $listicle_item->listicle_id = $listicle->id;
        $listicle_item->save();
      }
    }

    public function addItems(Request $request)
    {

      $listicle->items()->saveMany([
        'subtitle' => $request->subtitle,
        'image_url' => 'test.jpg',
        'body' => $request->items_body,
        'listicle_id' => $listicle->id
      ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
