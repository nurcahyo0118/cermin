<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListicleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listicle_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subtitle');
            $table->string('image_url');
            $table->string('video_url')->nullable();
            $table->text('body');
            $table->integer('listicle_id')->unsigned();
            $table->timestamps();

            $table->foreign('listicle_id')->references('id')->on('listicles');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listicle_items');
    }
}
