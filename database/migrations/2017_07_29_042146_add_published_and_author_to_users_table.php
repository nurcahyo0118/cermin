<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublishedAndAuthorToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('narrations', function (Blueprint $table) {
          $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING', 'REJECTED', 'REVISION'])->default('DRAFT');
          $table->integer('author_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('narrations', function (Blueprint $table) {
          $table->dropColumn('status');
          $table->dropColumn('author_id');
        });
    }
}
