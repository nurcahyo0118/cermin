<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Factory::create();

      Category::truncate();

      foreach (range(1,15) as $i) {
        Category::create([
          'name' => $faker->name,
          'description' => $faker->sentence
        ]);
      }
    }
}
