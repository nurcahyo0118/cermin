<?php

use Illuminate\Database\Seeder;
use App\Narration;
use Faker\Factory;

class NarrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Factory::create();

      Narration::truncate();

      foreach (range(1,15) as $i) {
        Narration::create([
          'title' => $faker->sentence,
          'excerpt' => $faker->sentence,
          'body' => $faker->sentence,
          'author_id' => '0'
        ]);
      }
    }
}
