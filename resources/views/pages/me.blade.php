@extends('main')
@section('content')
<div class="col-md-8">
  <div class="form-group">
    <label>Avatar</label>
    <div>
      <img src="/image/login.png" class="img-rounded" width="150">
      <ul style="display:inline-block;">
        <li>Upload a 500x500 pixel image to be used as your avatar</li>
        <li>Image should not exceed more than 1 MB</li>
        <li>Your new avatar will appear within 5 minutes</li>
      </ul>
    </div>
  </div>

  <div class="form-group">
    <label>Email</label>
    <input type="text" class="form-control" disabled>
  </div>

  <div class="form-group">
    <label>Username</label>
    <input type="text" class="form-control" disabled>
  </div>

  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="" value="">
  </div>

  <div class="form-group">
    <label>Gender</label>
    <input type="text" class="form-control" name="" value="">
  </div>

  <div class="form-group">
    <label>City</label>
    <input type="text" class="form-control" name="" value="">
  </div>

  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" rows="4" cols="80"></textarea>
  </div>

  <div class="form-group">
    <button class="btn btn-info"><strong>Save Profile</strong></button>
  </div>

</div>
<div class="col-md-4">
  <a href="#!" class="btn btn-info"><strong><i class="fa fa-list"></i> Create Listicle</strong></a>
  <a href="#!" class="btn btn-info"><strong><i class="fa fa-file-text-o"></i> Create Naration</strong></a>

  <div class="panel panel-default" style="margin-top:20px;">
    <img src="/image/login.png" class="img-rounded" width="150">
    <div style="display:inline-block;">
      <h3>Nur Cahyo Putro</h3>
      <p>Developer</p>
    </div>
  </div>
  <div class="panel panel-default text-center">
    <h1>Developer</h1>
  </div>
</div>
@endsection
