<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/cermin.css">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    </head>
    <body>
    <div class="container main-content">

        <h1>Cermin</h1>
      <div class="col-md-4">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="col-md-12">
              <div class="form-group">
                <label>username :</label>
                <input type="text" class="form-control" name="" value="">
              </div>
              <div class="form-group">
                <label>password :</label>
                <input type="password" class="form-control" name="" value="">
              </div>
              <div class="form-group">
                <a href="#!">Register new user</a>
                <button type="button" class="btn btn-default pull-right" name="button">Sign in</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <img src="/image/ecosystem.png" class="img-responsive pull-right" height="440">
      </div>
    </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
