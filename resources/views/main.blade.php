<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="/css/cermin.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
      <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />
      <script>window.Laravel = { csrfToken: '{ csrf_token() }'}</script>
      <meta name="_token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{mix('css/app.css')}}">


      <title>Laravel</title>

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,600" rel="stylesheet" type="text/css">

    </head>
    <body>

    <!-- <div id="front"></div> -->
    <div id="root"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- <script src="{{mix('js/frontend.js')}}" type="text/javascript"></script> -->
    <script src="{{mix('js/app.js')}}" type="text/javascript"></script>

    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyD-3BUhpg8CUq2NXHzMmbMef9tkIjLjdX0",
        authDomain: "cermin-ac29a.firebaseapp.com",
        databaseURL: "https://cermin-ac29a.firebaseio.com",
        projectId: "cermin-ac29a",
        storageBucket: "gs://cermin-ac29a.appspot.com/",
        messagingSenderId: "795373494318"
      };
      firebase.initializeApp(config);

      var fileButton = document.getElementById('fileButton');
      var fileInput = document.getElementById('fileInput');

      $('#fileButton').on('click', function(){

        console.log('NICE');
          var file = fileInput.files[0];
        // fileInput.addEventListener('change', function(e) {
        //   var file = e.target.files[0];
        //   // var file = $('#fileInput').val();
          var storageRef = firebase.storage().ref('images/' + file.name);
        //
          var task = storageRef.put(file);
        //
          task.on('state_changed',
          function progress(snapshot){
            $('#fileButton').button('loading');
          },
          function error(err){

          },
          function complete(){
            firebase.storage().ref().child('images/' + file.name).getDownloadURL().then(function(url) {
              document.getElementById("img-value").src = url;
              $('#exampleModal2').modal("show");
              $('#fileButton').button('reset');

              document.getElementById("cover-panel").style.display = "inline";
              document.getElementById("cover-panel").style.visibility = "visible";
              document.getElementById("cover-preview").src = url;
              document.getElementById("cover-input").style.display = "none";
              document.getElementById("cover-input").style.visibility = "hidden";

              // document.getElementById("div-img").style.height = "350px";
              // document.getElementById("div-img").style.width = "100vh";
              // document.getElementById("div-img").style.maxHeight = "350px";
              // document.getElementById("panel-img").style.background = "url("+url+") no-repeat center center";
            });



            // document.getElementById("img-value");



          });
        // });

      });

    </script>
    </body>
</html>
