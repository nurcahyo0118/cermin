import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import Listicle from '../views/listicle/Listicle.vue'
import CreateListicle from '../views/listicle/CreateListicle.vue'
import EditListicle from '../views/listicle/EditListicle.vue'

import Me from '../views/me/Me.vue'

import Narration from '../views/narration/Narration.vue'
import CreateNarration from '../views/narration/CreateNarration.vue'
import EditNarration from '../views/narration/EditNarration.vue'

import Home from '../views/frontend/Home.vue'
import Course from '../views/frontend/Course.vue'
import Show from '../views/frontend/Show.vue'
import Login from '../views/auth/Login.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', component: Home },
    { path: '/dashboard', component: Narration, name: 'dashboard', meta: { requireAuth: true } },
    { path: '/dashboard/me', component: Me },
    { path: '/dashboard/narration/create', component: CreateNarration },
    { path: '/dashboard/narration/:id/edit', component: EditNarration },
    { path: '/listicle', component: Listicle },
    { path: '/dashboard/listicle/create', component: CreateListicle },
    { path: '/dashboard/listicle/:id/edit', component: EditListicle },
    { path: '/course', component: Course },
    { path: '/:id', component: Show },
    { path: '/auth/login', component: Login, name: 'login' },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('authUser'))
    console.log(authUser.access_token)
    if (authUser && authUser.access_token) {
      next()
    }else {

      next('/auth/login')
    }
  }else {
    next()
  }
})


export default router
