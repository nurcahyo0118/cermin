import Vue from 'vue'

import VueResource from 'vue-resource'
import App from './App.vue'
import router from './router'
// Import and use Vue Froala lib.
import VueFroala from 'vue-froala-wysiwyg'
// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min')

// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')

window.$ = jQuery;

window.froala = require('vue-froala-wysiwyg');
window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest',
};

Vue.use(VueFroala)

const app = new Vue({
	el: '#root',
	template: `<app></app>`,
	components: { App },
	router,
  VueFroala
})
